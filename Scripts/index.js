﻿function setMsj(msj) {
    $('#pnlmsj').show();
    $('#lblmsj').text(msj);
}


$(document).ready(function () {

    $("#btnLogin").on('click', function () {
        var usr = $('#txtUser').val();
        var psw = $('#txtPsw').val();

        if (usr === '') { setMsj('Proporcione un usuario.'); return; }
        if (psw === '') { setMsj('Proporcione una contraseña.'); return; }
        if (psw.length < 7) { setMsj('Formato de contraseña incorrecto.'); return; }



        $.ajax({
            url: 'https://localhost:44380/loggin/' + usr + '/' + psw,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                var response = JSON.parse(data);
                if (response) {
                    switch (response.Value) {
                        case 'Bad psw':
                            setMsj('Contraseña erronea.');
                            break;
                        case 'Bad usr':
                            setMsj('El usuario no existe.');
                            break;
                        case 'Grant':
                            $('#pnlmsj').hide();
                            $('#pnlLogin').hide();
                            $('#pnlTabla').show();
                            break;
                        default:
                    }
                }
                else { setMsj('Error al autenticar.'); }
            }
        });
    });

    $('#btnProducts').on('click', function () {
        $.ajax({
            url: 'https://localhost:44380/products/',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                var response = JSON.parse(data);
                if (response) {
                    $("#tblProducts tbody").empty();
                    $.each(response.listProducts, function (i, row) {
                        var myRow = "<tr><td>" + row.Name + "</td><td>" + row.Sku + "</td><td>" + row.Description + "</td></tr>";
                        $("#tblProducts tbody").append(myRow);
                    });
                    
                }
                else { alert('Error al cargar productos.'); }
            }
        });
    });
});