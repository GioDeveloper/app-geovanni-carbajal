﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="AppData.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="Scripts/index.js" type="text/javascript"></script>
    <title>Products</title>
</head>
<body>
    <div id="pnlLogin" runat="server">
        <div class="container mt-5 text-center">
            <div class="row">
                <div class="form-group col-6">
                    <label for="txtUser">User</label>
                    <input type="text" class="form-control" id="txtUser" maxlength="8">
                </div>
                <div class="form-group col-6">
                    <label for="txtPsw">Password</label>
                    <input type="password" class="form-control" id="txtPsw" maxlength="7">
                </div>
                <div class="form-group col-12">
                    <button id="btnLogin" type="button" class="btn btn-primary">Ingresar</button>
                </div>
                <div class="form-group col-12">
                    <div id="pnlmsj" class="alert alert-warning" style="display: none" role="alert">
                        <label id="lblmsj"></label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="pnlTabla" style="display: none" runat="server">
        <div class="container mt-5 text-center">
            <div class="form-group col-12">
                <button id="btnProducts" type="button" class="btn btn-success">Ver Productos</button>
            </div>
            <table id="tblProducts" class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">SKU</th>
                        <th scope="col">Description</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>
